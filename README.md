# full-stack

#### 项目介绍
全栈meteor demo

#### 软件架构
1. nodejs + meteor(full-stack) + materialize => admin + app
2. i18n + accounts-ui + accounts-password
3. jquery + underscore.js + flow-router 
4. ...

#### 安装教程

1. mac :  curl https://install.meteor.com | sh
2. git clone https://gitee.com/jxe/full-stack.git
3. cd ./full-stack
4. meteor npm install
4. meteor