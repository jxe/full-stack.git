import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import { T9n } from 'meteor/softwarerero:accounts-t9n';
import { _ } from 'meteor/underscore';
import { $ } from 'meteor/jquery';

import './i18n.html'

Template.i18n.onCreated(function(){
  default_language='zh-CN'
  T9n.setLanguage(default_language);
  TAPi18n.setLanguage(default_language);
})

Template.i18n.helpers({
  languages() {
     return _.keys(TAPi18n.getLanguages());
   },
   isActiveLanguage(language) {
     return (TAPi18n.getLanguage() === language);
   },
})

Template.i18n.events({
  'click .js-toggle-language'(event) {
    const language = $(event.target).html().trim();
    //const shortLanguage = language.replace("-", "_").toLowerCase();
    console.log(language)
    T9n.setLanguage(language);
    TAPi18n.setLanguage(language);
  },
})
