import { AccountsTemplates } from 'meteor/useraccounts:core';

/**
 * The useraccounts package must be configured for both client and server to work properly.
 * See the Guide for reference (https://github.com/meteor-useraccounts/core/blob/master/Guide.md)
 */

AccountsTemplates.configure({
  showForgotPasswordLink: true,
  defaultTemplate: 'Auth_page', // 用户系统认证页面 为什么没有 Template Auth_page helper
  defaultLayout: 'App_body', //与  template name = 'App_body' 容器对应
  defaultContentRegion: 'main',
  defaultLayoutRegions: {},
  enablePasswordChange : true, //可以更改密码
  showResendVerificationEmailLink:true, //是否显示发送邮件验证码
  /* public: {
        "reCaptcha": {
            "siteKey": 'jxtpro-siteKey'
        }
  }, */
  reCaptcha: {
        siteKey:'jxtpro-siteKey',
        theme: "light",
        data_type: "image"
    },
  showReCaptcha: false,
});

AccountsTemplates.configureRoute('signIn', {
  name: 'signin',
  path: '/signin',
  redirect: '/admin',
});

AccountsTemplates.configureRoute('signUp', {
  name: 'join',
  path: '/join',
});

AccountsTemplates.configureRoute('forgotPwd');

AccountsTemplates.configureRoute('resetPwd', {
  name: 'resetPwd',
  path: '/reset-password',
});
