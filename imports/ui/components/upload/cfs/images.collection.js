var imageStore = new FS.Store.FileSystem("images", {
  path: "/Users/x/Desktop/meteor_uploads",
  maxTries: 1
});

YourFileCollection = new FS.Collection("YourFileCollection", {
    stores: [imageStore,
      /* new FS.Store.FileSystem("yourFileCollection", {path: "/Users/x/Desktop/meteor_uploads"}), */
      /* new FS.Store.GridFS('EntriesImages') */
    ]

});

YourFileCollection.deny({
  insert: function() {
    return false;
  },
  update: function() {
    return false;
  },
  remove: function() {
    return false;
  },
  download: function() {
    return false;
  }
});

YourFileCollection.allow({
  insert: function() {
    return true;
  },
  update: function() {
    return true;
  },
  remove: function() {
    return true;
  },
  download: function() {
    return true;
  }
});
