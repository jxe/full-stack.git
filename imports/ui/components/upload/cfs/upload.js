import './upload.html'


Meteor.subscribe("fileUploads");

Template.upload.events({
    'click #deleteFileButton ': function (event) {
      YourFileCollection.remove({_id:this._id});
    },
    'change .your-upload-class': function (event, template) {
      FS.Utility.eachFile(event, function (file) {
        var yourFile = new FS.File(file);
        yourFile.creatorId = 123; // todo

        YourFileCollection.insert(yourFile, function (err, fileObj) {
          if (err) {
            if(Meteor.isClient()){
              alert(err)
            }else{
              console.log(err)
            }
          }
        });

      });
    }
});

Template.upload.helpers({
  theFiles: function () {
    /* console.log(YourFileCollection.find()) */
    return YourFileCollection.find();
  }
});
