S3.config = {
	key: 'amazonKey',
	secret: 'amazonSecret',
	bucket: 'bucketName',
	region: 'eu-west-1' // Only needed if not "us-east-1" or "us-standard"
};
