/* global alert */
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Template } from 'meteor/templating';
import { ActiveRoute } from 'meteor/zimme:active-route';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { TAPi18n } from 'meteor/tap:i18n';
import { T9n } from 'meteor/softwarerero:accounts-t9n';
import { _ } from 'meteor/underscore';
import { $ } from 'meteor/jquery';

import './login0.html'


Template.login.onCreated(function appBodyOnCreated() {

  /* TODO 订阅数据 */
  /* this.subscribe('lists.public');
  this.subscribe('roles');
  this.subscribe('lists.private'); */

  this.state = new ReactiveDict();
  this.state.setDefault({
    menuOpen: false,
    userMenuOpen: false,
  });
});

Template.login.helpers({
  menuOpen() {
      const instance = Template.instance();
      return instance.state.get('menuOpen') && 'menu-open';
  },
  cordova() {
      return Meteor.isCordova && 'cordova';
  },
  emailLocalPart() {
      const email = Meteor.user().emails[0].address;
      return email.substring(0, email.indexOf('@'));
    },
    userMenuOpen() {
      const instance = Template.instance();
      return instance.state.get('userMenuOpen');
    },connected() {
    if (showConnectionIssue.get()) {
      return Meteor.status().connected;
    }

    return true;
  },
  templateGestures: {
    'swipeleft .cordova'(event, instance) {
      instance.state.set('menuOpen', false);
    },
    'swiperight .cordova'(event, instance) {
      instance.state.set('menuOpen', true);
    },
  },
})

Template.login.events({
  'click .js-menu'(event, instance) {
    instance.state.set('menuOpen', !instance.state.get('menuOpen'));
  },

  'click .content-overlay'(event, instance) {
    instance.state.set('menuOpen', false);
    event.preventDefault();
  },

  'click .js-user-menu'(event, instance) {
    instance.state.set('userMenuOpen', !instance.state.get('userMenuOpen'));
    // stop the menu from closing
    event.stopImmediatePropagation();
  },

  'click #menu a'(event, instance) {
    instance.state.set('menuOpen', false);
  },

  'click .js-logout'() {
    Meteor.logout();
  },

})
