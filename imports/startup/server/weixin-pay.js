/* 参考 https://github.com/tvrcgo/weixin-pay */
function setupConfig() {
  var WXPay = require('weixin-pay');
  wxpay = WXPay({
  	appid: 'xxxxxxxx',
  	mch_id: '1234567890',
  	partner_key: 'xxxxxxxxxxxxxxxxx', //微信商户平台API密钥
  	pfx: fs.readFileSync('./wxpay_cert.p12'), //微信商户平台证书
  });
}

Meteor.startup(() => {
  setupConfig()
})
