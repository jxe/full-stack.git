/* 参考 https://github.com/mhurwi/meteor-with-express-example */

import testRouter from '../../../imports/api/restful-api/test/test';

function setupApi() {
  const express = require('express');
  const app = express();
  const bodyParser = require('body-parser')
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use('/api',testRouter)
  app.get('/api', (req, res) => {
    res.status(200).json({ message: 'Hello World!!!' });
  });
  WebApp.connectHandlers.use(app);
}

Meteor.startup(() => {
  setupApi()
})
