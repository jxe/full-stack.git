function executeBaiDuMapJs(){
  var map = new BMap.Map("container");
  var point = new BMap.Point(116.404, 39.915);
  map.centerAndZoom(point, 15);
  window.setTimeout(function(){
      map.panTo(new BMap.Point(116.409, 39.918));
  }, 2000);
}


function dynamicLoadJs(url, callback) {
  var head = document.getElementById('baiduMapHead');
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = url;
  if(typeof(callback)=='function'){
      script.onload = script.onreadystatechange = function () {
          if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete"){
              callback();
              script.onload = script.onreadystatechange = null;
          }
      };
  }
  head.appendChild(script);
}
